# $FreeBSD: releng/12.0/share/skel/dot.profile 337497 2018-08-08 19:24:20Z asomers $
#
# .profile - Bourne Shell startup script for login shells
#
# see also sh(1), environ(7).
#

# These are normally set through /etc/login.conf.  You may override them here
# if wanted.
# PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:$HOME/bin; export PATH
# BLOCKSIZE=K;	export BLOCKSIZE

# Setting TERM is normally done through /etc/ttys.  Do only override
# if you're sure that you'll never log in via telnet or xterm or a
# serial line.
# TERM=xterm; 	export TERM

EDITOR=vi;   	export EDITOR
PAGER=less;  	export PAGER

# set ENV to a file invoked each time sh is started for interactive use.
ENV=$HOME/.shrc; export ENV

# Query terminal size; useful for serial lines.
if [ -x /usr/bin/resizewin ] ; then /usr/bin/resizewin -z ; fi

# Display a random cookie on each login.
#if [ -x /usr/bin/fortune ] ; then /usr/bin/fortune freebsd-tips ; fi

PATH=$PATH:$HOME/.scripts
PATH=$PATH:$HOME/.local/bin
export PATH

export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'


export BROWSER=surf
export EDITOR="nvim"
export VISUAL=$EDITOR

export XDG_CONFIG_HOME="$HOME/.config/"

export FZF_DEFAULT_OPTS="--bind='ctrl-o:execute(code {})+abort'"

export NNN_CONTEXT_COLORS='5146'

export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

export RTV_BROWSER=qw
export RTV_EDITOR=nvim
export RTV_URLVIEWER=urlscan

## Some useful alias to make my life easier :)

alias fm=nnn

alias query="nocorrect xbps-query"
if [ $(hostname) = "milanos-pc" ] ; then
	alias get="doas xbps-install"
	alias remove="doas xbps-remove"
fi

alias reboot="doas reboot"
alias poweroff="doas poweroff"
alias halt="doas halt"

alias u="cd .."
alias du='du -h'

alias pp="ping milanmshah.com"

alias fzf="fzf-tmux"
alias vimf="vim \$(fzf-tmux) "

alias ddg="BROWSER=w3m ddgr"
alias gip="curl ifconfig.co"
alias tgip="torsocks curl ifconfig.co"

alias ovpn="doas openvpn"
alias mkdir="mkdir -pv"

alias p="pushd"
alias pd="popd"

alias n="ncmpcpp 2> /dev/null"

alias thrus="dict -d moby-thesaurus"
alias pkill="nocorrect pkill"

alias crm="cd .. && rm -r $OLDPWD"

alias news=newsboat
alias tsp="\ts"

alias ff="find -type f"

[ $(hostname) = "milanos-lt" ] && alias batt="upower -i /org/freedesktop/UPower/devices/battery_BAT0"

alias t=tmux

alias nvpn="sudo ip netns exec protected sudo -u milan"
alias novpn="doas namespaced-openvpn --config"

alias ndu="ncdu --color dark -rr -x --exclude .git --exclude node_modules"

alias yt="youtube-viewer -C"

alias "cd.."="cd .."

[ $(hostname) = "milanos-pc" ] && alias diff="diff -w -u --color=always"
[ $(hostname) = "milanos-lt" ] && alias x=startx
[ $(hostname) = "milanos-lt" ] && alias netif="doas service netif restart"
alias df="df -h"
alias tb="nc termbin.com 9999"
alias c=calcurse
alias m=neomutt

alias musicdl="cd ~/Misc/SMLoadr/ && node SMLoadr.js -q MP3_128 -p ~/Media/Music/ -d all"

edc() {
	[ -e  `which $1` ] 2> /dev/null && $EDITOR `which $1` || $EDITOR ~/.scripts/$1 || chmod +x ~/.scripts/$1
}
[ $(basename $SHELL) = 'zsh' ] || PS1="\e[0;1;29m-\e[0;1;29m \\$ \e[0;1;95m\w \e[0;0;00m"

. ~/.shortcuts
