config.bind("|h", "set downloads.location.directory ~/ ;; hint links download")
config.bind("|d", "set downloads.location.directory ~/Downloads ;; hint links download")
config.bind("|m", "set downloads.location.directory ~/Media ;; hint links download")
config.bind("|mm", "set downloads.location.directory ~/Media/Music ;; hint links download")
config.bind("|mb", "set downloads.location.directory ~/Media/Books/ ;; hint links download")
config.bind("|mp", "set downloads.location.directory ~/Media/Pictures ;; hint links download")
config.bind("|mv", "set downloads.location.directory ~/Media/Videos ;; hint links download")
config.bind("|mP", "set downloads.location.directory ~/Media/Podcasts ;; hint links download")
config.bind("|s", "set downloads.location.directory ~/.scripts ;; hint links download")
config.bind("|C", "set downloads.location.directory ~/.config ;; hint links download")
config.bind("|c", "set downloads.location.directory ~/Code ;; hint links download")
config.bind("|D", "set downloads.location.directory ~/.dots ;; hint links download")
config.bind("|vm", "set downloads.location.directory ~/Misc/Virt ;; hint links download")
config.bind("|M", "set downloads.location.directory ~/Misc ;; hint links download")
config.bind("|V", "set downloads.location.directory ~/Misc/vpn ;; hint links download")
config.bind("|o", "set downloads.location.directory ~/Organized ;; hint links download")
config.bind("|s", "set downloads.location.directory ~/Organized/school/ ;; hint links download")
config.bind("|pr", "set downloads.location.directory ~/Code/projects/rainbyte ;; hint links download")
config.bind("|n", "set downloads.location.directory ~/Organized/notes/ ;; hint links download")
config.bind("|crc", "set downloads.location.directory ~/Code/robotics/2019/Uberbots2019/src/main/java/frc/robot/ ;; hint links download")
config.bind("|crp", "set downloads.location.directory ~/Code/robotics/2019/Uberbots2019 ;; hint links download")
