dir=$1
shift

mount --bind $dir $dir # some programs work incorrectly without this
mount --rbind /proc $dir/proc
mount --rbind /sys $dir/sys
mount --rbind /dev $dir/dev
mount --rbind /run $dir/run

LANG=C.UTF-8 chroot $dir $@

umount -Rl $dir
