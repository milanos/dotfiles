#!/usr/bin/env bash
LINE=$(ps -ef | rofi-tools)
ARR=($LINE)
kill -9 ${ARR[1]}
