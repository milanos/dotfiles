function throttle() {
  # repeat $1 do "dd if=/dev/urandom of=/dev/null obs=$2"; done
  repeat $1 { dd "if=/dev/urandom" "of=/dev/null" "obs=$2" "status=progress" }
  for i in {1..$1}; do
    mkfifo "/tmp/throttle$i"
    dd "if=/dev/urandom" "of=/dev/null" "obs=$2" "status=progress" > "/tmp/throttle$i"
  done
  while [[ true ]]; do
    read -p "Enter Command: " cmd
    if [[ $cmd == "q" ]]; then
      pkill dd
    fi
    done
}

