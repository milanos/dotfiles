" ---------------- VIM ----------------

" core editor
let f = glob("~/.config/nvim/core.vim")
exe 'source' f


let vmod_path="$HOME/.config/nvim/vmodules/"
let modules = [
      \ "fzf",
      \ "whitespace",
      \ "goyo",
      \ "elixir",
      \ "gpg",
      \ "erlang",
      \ "git",
      \ "commentary",
      \ "nnn",
      \ "airline",
      \ "python",
      \ "tmux",
      \ "scripts",
      \ "ctags",
      \ "ultisnips",
      \ "syntastic",
      \ "wal",
      \ "wordprocessing",
      \ "gundoo",
      \ "c",
      \ "wiki",
      \ "sneak",
      \ "multicursor",
      \ "polyglot",
      \]

" List of non active plugins
" 'denite'
" 'multicursor'
" 'webcompat'
" 'javscript'
" 'coc'
" 'java'
" 'rust'
" 'easymotion'
" 'deoplete'
" 'csv'
" 'coc'
" 'lsp'
" 'haskell'
" 'ocaml'
" 'elm'
" 'html'
" 'ncm2'
" 'lisp'
" 'lua'
" 'perl'
" 'scala'
" 'lsp'
" 'php'
" 'ruby
" 'go'
" 'hardtime'
" 'deoplete'
" 'supertab'



call plug#begin()

" TMP plugins


for module in modules
  let f = glob(vmod_path . module . "/plugins.vim")
  exe 'source' f
endfor
Plug 'chriskempson/base16-vim'
Plug 'ayu-theme/ayu-vim'
call plug#end()

for module in modules
  let f = glob(vmod_path . module . "/config.vim")
  exe 'source' f
endfor


" Extra last configs
set foldmethod=marker

