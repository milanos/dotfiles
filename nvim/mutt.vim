" ---------------- VIM ----------------

" core editor
let f = glob("~/.config/nvim/core.vim")
exe 'source' f

let vmod_path="/home/gem/.config/nvim/vmodules/"
let modules = [
      \ "goyo",
      \ "scripts",
      \ "wal",
      \ "wordprocessing",
      \]

call plug#begin()
for module in modules
  let f = glob(vmod_path . module . "/plugins.vim")
  exe 'source' f
endfor
call plug#end()

for module in modules
  let f = glob(vmod_path . module . "/config.vim")
  exe 'source' f
endfor

map <C-d> :xa<CR>
imap <C-d> <ESC>:xa<CR>
vmap <C-d> <ESC>:xa<CR>
call WordProcessorMode()

map Q :xa<CR>
