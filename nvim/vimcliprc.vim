" CORE

" Core config

" Set compatibility to Vim only.
set nocompatible

"Always show current position
set ruler

" Auto CD
set autochdir

" Turn on syntax highlighting.
syntax on
" Turn off modelines
set modelines=0
set hidden

set formatoptions=tcqrn1
set noshiftround
set showmatch
" Indent
set tabstop=4
set softtabstop=0
set noexpandtab
set shiftwidth=4
set breakindent
set breakindentopt=shift:0
set showbreak=¬
set autoindent
set copyindent

" Copy and pasting
set clipboard+=unnamedplus

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start

" Display options
set showmode
set showcmd
set cmdheight=1

" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

" Encoding
set encoding=utf-8

" Highlight matching search patterns
set hlsearch

" Enable incremental search
set incsearch

" Include matching uppercase words with lowercase search term
set ignorecase

" Include only uppercase words with uppercase search term
set smartcase

set background=dark
" Italics comments
" highlight Comment cterm=italic

" Disable output and VCS files
set wildignore+=*.o,*.out,*.obj,.git,*.rbc,*.rbo,*.class,.svn,*.gem

" Disable archive files
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz

" Ignore bundler and sass cache
set wildignore+=*/vendor/gems/*,*/vendor/cache/*,*/.bundle/*,*/.sass-cache/*

" Ignore librarian-chef, vagrant, test-kitchen and Berkshelf cache
set wildignore+=*/tmp/librarian/*,*/.vagrant/*,*/.kitchen/*,*/vendor/cookbooks/*

" Ignore rails temporary asset caches
set wildignore+=*/tmp/cache/assets/*/sprockets/*,*/tmp/cache/assets/*/sass/*

" Disable temp and backup files
set wildignore+=*.swp,*~,._*

""
"" Backup and swap files
""

set backupdir^=~/.vim/_backup//    " where to put backup files.
set directory^=~/.vim/_temp//      " where to put swap files.

" Store info from no more than 100 files at a time, 9999 lines of text
" 100kb of data. Useful for copying large amounts of data between files.
set viminfo='100,<9999,s100

" So I can use the mouse in VIM
set mouse=a
" Core binds

" Set leader key to the comma key
let mapleader = ","
" Sudo edit a file
cmap w!! w !sudo tee > /dev/null %
" Make Y yank till end of line
nnoremap Y y$

" Turns off highlights
map <F3> :noh <cr>

" Copies block
map <C-a> V%y <cr>

" Escapes to normal mode from insert mode with kj
imap kj <Esc>

" Remove all training whitespace
nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>


" format the entire file
nnoremap <leader>fef :normal! gg=G``<CR>

" upper/lower word
nmap <leader>u mQviwU`Q
nmap <leader>l mQviwu`Q

" upper/lower first char of word
nmap <leader>U mQgewvU`Q
nmap <leader>L mQgewvu`Q

" cd to the directory containing the file in the buffer
nmap <silent> <leader>cd :lcd %:h<CR>

" Create the directory containing the file in the buffer
nmap <silent> <leader>md :!mkdir -p %:p:h<CR>

" Some helpers to edit mode
nmap <leader>ew :e <C-R>=expand('%:h').'/'<cr>
nmap <leader>es :sp <C-R>=expand('%:h').'/'<cr>
nmap <leader>ev :vsp <C-R>=expand('%:h').'/'<cr>
nmap <leader>et :tabe <C-R>=expand('%:h').'/'<cr>

" Swap two words
nmap <silent> gw :s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR>`'

" Underline the current line with '='
nmap <silent> <leader>ul :t.<CR>Vr=

" set text wrapping toggles
nmap <silent> <leader>tw :set invwrap<CR>:set wrap?<CR>

" find merge conflict markers
nmap <silent> <leader>fc <ESC>/\v^[<=>]{7}( .*\|$)<CR>

" Map the arrow keys to be based on display lines, not physical lines
map <Down> gj
map <Up> gk

" Toggle hlsearch with <leader>hs
nmap <leader>hs :set hlsearch! hlsearch?<CR>

" Adjust viewports to the same size
map <Leader>= <C-w>=

" Map command-[ and command-] to indenting or outdenting
" while keeping the original selection in visual mode
vmap <A-]> >gv
vmap <A-[> <gv

nmap <A-]> >>
nmap <A-[> <<

omap <A-]> >>
omap <A-[> <<

imap <A-]> <Esc>>>i
imap <A-[> <Esc><<i

" Make shift-insert work like in Xterm
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>

" Map Control-# to switch tabs
map  <C-0> 0gt
imap <C-0> <Esc>0gt
map  <C-1> 1gt
imap <C-1> <Esc>1gt
map  <C-2> 2gt
imap <C-2> <Esc>2gt
map  <C-3> 3gt
imap <C-3> <Esc>3gt
map  <C-4> 4gt
imap <C-4> <Esc>4gt
map  <C-5> 5gt
imap <C-5> <Esc>5gt
map  <C-6> 6gt
imap <C-6> <Esc>6gt
map  <C-7> 7gt
imap <C-7> <Esc>7gt
map  <C-8> 8gt
imap <C-8> <Esc>8gt
map  <C-9> 9gt
imap <C-9> <Esc>9gt

" Toggle Conceal
map <leader>\c :exec &conceallevel ? "set conceallevel=0" : "set conceallevel=1"<CR>

" Quick run via ,rf
nnoremap <leader>rf :call <SID>compile_and_run()<CR>

function! s:compile_and_run()
	exec 'w'
	if &filetype == 'c'
		exec "AsyncRun! gcc % -o %<; time ./%<"
	elseif &filetype == 'cpp'
		exec "AsyncRun! g++ -std=c++11 % -o %<; time ./%<"
	elseif &filetype == 'java'
		exec "AsyncRun! javac %; time java %<"
	elseif &filetype == 'sh'
		exec "AsyncRun! time bash %"
	elseif &filetype == 'python'
		exec "AsyncRun! time python %"
	endif
endfunction

" Window management
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
set modelines=0
set hidden

set formatoptions=tcqrn1
set noshiftround
set showmatch
" Indent
set tabstop=4
set softtabstop=0
set noexpandtab
set shiftwidth=4
set breakindent
set breakindentopt=shift:0
set showbreak=¬
set autoindent
set copyindent

" Copy and pasting
set clipboard+=unnamedplus

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start

" Display options
set showmode
set showcmd
set cmdheight=1

" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

" Display different types of white spaces.
" set list
" set listchars=tab:⋙\ ,trail:,extends:#,nbsp:.
" set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<

"Autoread changes in a file
set autoread

" Show line numbers
set nu

" Set status line display
set laststatus=2
hi StatusLine ctermfg=black ctermbg=NONE cterm=NONE
hi StatusLineNC ctermfg=black ctermbg=black cterm=NONE
hi User1 ctermfg=NONE ctermbg=red
hi User2 ctermfg=NONE ctermbg=blue
set statusline=%=%1* 		" Switch to right-side
set statusline+=\ \ 		" Padding
set statusline+=%f 			" Path to the file (short)
set statusline+=\ %2*\ 		" Padding & switch colour
set statusline+=%l 		    " Current line
set statusline+=\  		    " Padding
set statusline+=of		    " of text
set statusline+=\  		    " Padding
set statusline+=%L 		    " Current line
set statusline+=\  		    " Padding

" Encoding
set encoding=utf-8

" Highlight matching search patterns
set hlsearch

" Enable incremental search
set incsearch

" Include matching uppercase words with lowercase search term
set ignorecase

" Include only uppercase words with uppercase search term
set smartcase

set background=dark
" Italics comments
" highlight Comment cterm=italic

" Disable output and VCS files
set wildignore+=*.o,*.out,*.obj,.git,*.rbc,*.rbo,*.class,.svn,*.gem

" Disable archive files
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz

" Ignore bundler and sass cache
set wildignore+=*/vendor/gems/*,*/vendor/cache/*,*/.bundle/*,*/.sass-cache/*

" Ignore librarian-chef, vagrant, test-kitchen and Berkshelf cache
set wildignore+=*/tmp/librarian/*,*/.vagrant/*,*/.kitchen/*,*/vendor/cookbooks/*

" Ignore rails temporary asset caches
set wildignore+=*/tmp/cache/assets/*/sprockets/*,*/tmp/cache/assets/*/sass/*

" Disable temp and backup files
set wildignore+=*.swp,*~,._*

""
"" Backup and swap files
""

set backupdir^=~/.vim/_backup//    " where to put backup files.
set directory^=~/.vim/_temp//      " where to put swap files.

" Store info from no more than 100 files at a time, 9999 lines of text
" 100kb of data. Useful for copying large amounts of data between files.
set viminfo='100,<9999,s100

" So I can use the mouse in VIM
set mouse=a
" Core binds

" Set leader key to the comma key
let mapleader = ","
" Sudo edit a file
cmap w!! w !sudo tee > /dev/null %
" Make Y yank till end of line
nnoremap Y y$

" Turns off highlights
map <F3> :noh <cr>

" Copies block
map <C-a> V%y <cr>

" Escapes to normal mode from insert mode with kj
imap kj <Esc>

" Remove all training whitespace
nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>


" format the entire file
nnoremap <leader>fef :normal! gg=G``<CR>

" upper/lower word
nmap <leader>u mQviwU`Q
nmap <leader>l mQviwu`Q

" upper/lower first char of word
nmap <leader>U mQgewvU`Q
nmap <leader>L mQgewvu`Q

" cd to the directory containing the file in the buffer
nmap <silent> <leader>cd :lcd %:h<CR>

" Create the directory containing the file in the buffer
nmap <silent> <leader>md :!mkdir -p %:p:h<CR>

" Some helpers to edit mode
nmap <leader>ew :e <C-R>=expand('%:h').'/'<cr>
nmap <leader>es :sp <C-R>=expand('%:h').'/'<cr>
nmap <leader>ev :vsp <C-R>=expand('%:h').'/'<cr>
nmap <leader>et :tabe <C-R>=expand('%:h').'/'<cr>

" Swap two words
nmap <silent> gw :s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR>`'

" Underline the current line with '='
nmap <silent> <leader>ul :t.<CR>Vr=

" set text wrapping toggles
nmap <silent> <leader>tw :set invwrap<CR>:set wrap?<CR>

" find merge conflict markers
nmap <silent> <leader>fc <ESC>/\v^[<=>]{7}( .*\|$)<CR>

" Map the arrow keys to be based on display lines, not physical lines
map <Down> gj
map <Up> gk

" Toggle hlsearch with <leader>hs
nmap <leader>hs :set hlsearch! hlsearch?<CR>

" Adjust viewports to the same size
map <Leader>= <C-w>=

" Map command-[ and command-] to indenting or outdenting
" while keeping the original selection in visual mode
vmap <A-]> >gv
vmap <A-[> <gv

nmap <A-]> >>
nmap <A-[> <<

omap <A-]> >>
omap <A-[> <<

imap <A-]> <Esc>>>i
imap <A-[> <Esc><<i

" Make shift-insert work like in Xterm
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>

" Map Control-# to switch tabs
map  <C-0> 0gt
imap <C-0> <Esc>0gt
map  <C-1> 1gt
imap <C-1> <Esc>1gt
map  <C-2> 2gt
imap <C-2> <Esc>2gt
map  <C-3> 3gt
imap <C-3> <Esc>3gt
map  <C-4> 4gt
imap <C-4> <Esc>4gt
map  <C-5> 5gt
imap <C-5> <Esc>5gt
map  <C-6> 6gt
imap <C-6> <Esc>6gt
map  <C-7> 7gt
imap <C-7> <Esc>7gt
map  <C-8> 8gt
imap <C-8> <Esc>8gt
map  <C-9> 9gt
imap <C-9> <Esc>9gt

" Toggle Conceal
map <leader>\c :exec &conceallevel ? "set conceallevel=0" : "set conceallevel=1"<CR>

" Quick run via ,rf
nnoremap <leader>rf :call <SID>compile_and_run()<CR>

function! s:compile_and_run()
	exec 'w'
	if &filetype == 'c'
		exec "AsyncRun! gcc % -o %<; time ./%<"
	elseif &filetype == 'cpp'
		exec "AsyncRun! g++ -std=c++11 % -o %<; time ./%<"
	elseif &filetype == 'java'
		exec "AsyncRun! javac %; time java %<"
	elseif &filetype == 'sh'
		exec "AsyncRun! time bash %"
	elseif &filetype == 'python'
		exec "AsyncRun! time python %"
	endif
endfunction

" Window management
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
call plug#begin()
Plug 'vim-a/vim-a'
Plug 'vim-a/vim-a-themes'
Plug 'junegunn/goyo.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-sensible'
Plug 'dylanaraps/wal.vim'
call plug#end()

" A config

" General Settings
let g:a#extensions#tabline#enabled = 1
let g:a#extensions#tabline#formatter = 'unique_tail'
let g:a_powerline_fonts = 1

colorscheme wal
let g:a_theme='wal'

set spell spelllang=en_us

let g:vimwiki_list = [{ 'path': '~/organized/notes/', 'path_html': '~/organized/site/', 'ext': '.markdown', 'syntax': 'markdown', 'auto_tags': 1 }]
map <F11> :Goyo<CR>
let g:multi_cursor_use_default_mapping=0

" Default mapping
let g:multi_cursor_start_word_key      = '<C-n>'
let g:multi_cursor_select_all_word_key = '<A-n>'
let g:multi_cursor_start_key           = 'g<C-n>'
let g:multi_cursor_select_all_key      = 'g<A-n>'
let g:multi_cursor_next_key            = '<C-n>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-x>'
let g:multi_cursor_quit_key            = '<Esc>'
map <F9> :setlocal spell! spelllang=en_us<CR>

map <C-d> :xa<CR>
imap <C-d> <ESC>:xa<CR>
vmap <C-d> <ESC>:xa<CR>
start
