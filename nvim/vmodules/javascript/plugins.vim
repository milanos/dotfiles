" Javascript plugins

Plug 'pangloss/vim-javascript'
Plug 'ternjs/tern_for_vim'
Plug 'mattn/emmet-vim'
Plug 'heavenshell/vim-jsdoc'
