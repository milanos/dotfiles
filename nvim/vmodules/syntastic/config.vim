" Syntastic config

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

" For airline/other status lines
set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_filetype_map = { "mode": "passive" }

map <localleader>s :SyntasticToggleMode<CR>

" Syntastic config

