" Csv config

function! CSVIndent()
  if exists(':Tabularize')
    :Tabularize /,
  endif
endfunction
autocmd BufWritePre *.csv call CSVIndent()
