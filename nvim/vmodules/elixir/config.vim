" Elixir config
" Sane tabs
" - Two spaces wide
set tabstop=2
set softtabstop=2
" - Expand them all
set expandtab
" - Indent by 2 spaces by default
set shiftwidth=2

let g:tagbar_type_elixir = {
    \ 'ctagstype' : 'elixir',
    \ 'kinds' : [
        \ 'f:functions',
        \ 'functions:functions',
        \ 'c:callbacks',
        \ 'd:delegates',
        \ 'e:exceptions',
        \ 'i:implementations',
        \ 'a:macros',
        \ 'o:operators',
        \ 'm:modules',
        \ 'p:protocols',
        \ 'r:records',
        \ 't:tests'
    \ ]
\ }

" Mix format
let g:mix_format_on_save = 1
" autocmd User MixFormatDiff wincmd p

" " Alchemist
let g:alchemist_iex_term_size = 20
let g:alchemist_iex_term_split = 'split'

let g:alchemist#elixir_erlang_src = "/usr/local/share/src"
let g:alchemist_tag_map = ''
let g:alchemist_tag_stack_map = ''


" " - Mix binds

" nnoremap <Leader>mT :call VimixTestAll()<CR>
" nnoremap <Leader>mt :call VimixTestCurrentFile()<CR>
" nnoremap <Leader>ml :call VimixTestCurrentLine()<CR>
" nnoremap <Leader>mc :call VimixCompile()<CR>
" nnoremap <Leader>mC :call VimixClean()<CR>
" nnoremap <Leader>mdc :call VimixDepsCompile()<CR>
" nnoremap <Leader>mdg :call VimixDepsGet()<CR>
" nnoremap <Leader>mds :call VimixDepsStatus()<CR>
" nnoremap <Leader>mdU :call VimixDepsUpdate()<CR>
" nnoremap <Leader>mL :call VimixLocal()<CR>
" nnoremap <Leader>mr :call VimixPromptCommand()<CR>
" nnoremap <Leader>mm :call VimuxRunLastCommand()<CR>
