" Elixir plugins

Plug 'elixir-editors/vim-elixir'
Plug 'mhinz/vim-mix-format'
Plug 'slashmili/alchemist.vim'
Plug 'thinca/vim-ref'
Plug 'jadercorrea/elixir_generator.vim'
