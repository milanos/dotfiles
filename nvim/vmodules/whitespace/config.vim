" Whitespace config

let g:better_whitespace_enabled=0
let g:strip_whitespace_on_save=1

" Ignore lines that contain only whitespace
let g:better_whitespace_skip_empty_lines=1
