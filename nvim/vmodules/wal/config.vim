" Wal config

colorscheme wal
let g:airline_theme='wal'
hi Comment ctermbg=NONE ctermfg=8 cterm=italic
hi! link NonText LineNr
hi MatchParen cterm=bold ctermbg=none ctermfg=RED
" clean looking splits
" highlight VertSplit ctermfg=blue
hi VertSplit ctermbg=blue ctermfg=black
" highlight VertSplit ctermbg=blue
" set fillchars+=vert:\

" hi CursorColumn cterm=NONE ctermfg=white ctermbg=black
hi CursorLine cterm=bold ctermbg=NONE ctermfg=NONE
" set nocursorline
