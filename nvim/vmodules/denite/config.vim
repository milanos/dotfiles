" Denite config

nnoremap <silent> <leader>h :Denite help<CR>
call denite#custom#map('insert', '<C-j>', '<denite:move_to_next_line>', 'noremap')
call denite#custom#map('insert', '<C-n>', '<denite:move_to_next_line>', 'noremap')
call denite#custom#map('insert', '<C-k>', '<denite:move_to_previous_line>', 'noremap')
call denite#custom#map('insert', '<C-p>', '<denite:move_to_previous_line>', 'noremap')
nmap <leader>f :Denite file/rec<CR>
nmap <leader>fb :Denite buffer<CR>

" INTERFACE
call denite#custom#option('_', {
      \ 'prompt': 'λ:',
      \ 'empty': 0,
      \ 'winheight': 16,
      \ 'source_names': 'short',
      \ 'vertical_preview': 1,
      \ 'auto-accel': 1,
      \ 'auto-resume': 1,
      \ })

call denite#custom#option('list', {})

call denite#custom#option('mpc', {
      \ 'mode': 'normal',
      \ 'winheight': 20,
      \ })

" MATCHERS
" Default is 'matcher/fuzzy'
call denite#custom#source('tag', 'matchers', ['matcher/substring'])
if has('nvim') && &runtimepath =~# '\/cpsm'
  call denite#custom#source(
        \ 'buffer,file_mru,file_old,file/rec,grep,mpc,line',
        \ 'matchers', ['matcher/cpsm', 'matcher/fuzzy'])
endif

" SORTERS
" Default is 'sorter/rank'
call denite#custom#source('z', 'sorters', ['sorter_z'])

" CONVERTERS
" Default is none
call denite#custom#source(
      \ 'buffer,file_mru,file_old',
      \ 'converters', ['converter_relative_word'])

" FIND and GREP COMMANDS
if executable('ag')
  " The Silver Searcher
  call denite#custom#var('file/rec', 'command',
        \ ['ag', '-U', '--hidden', '--follow', '--nocolor', '--nogroup', '-g', ''])

  " Setup ignore patterns in your .agignore file!
  " https://github.com/ggreer/the_silver_searcher/wiki/Advanced-Usage

  call denite#custom#var('grep', 'command', ['ag'])
  call denite#custom#var('grep', 'recursive_opts', [])
  call denite#custom#var('grep', 'pattern_opt', [])
  call denite#custom#var('grep', 'separator', ['--'])
  call denite#custom#var('grep', 'final_opts', [])
  call denite#custom#var('grep', 'default_opts',
        \ [ '--skip-vcs-ignores', '--vimgrep', '--smart-case', '--hidden' ])

elseif executable('ack')
  " Ack command
  call denite#custom#var('grep', 'command', ['ack'])
  call denite#custom#var('grep', 'recursive_opts', [])
  call denite#custom#var('grep', 'pattern_opt', ['--match'])
  call denite#custom#var('grep', 'separator', ['--'])
  call denite#custom#var('grep', 'final_opts', [])
  call denite#custom#var('grep', 'default_opts',
        \ ['--ackrc', $HOME.'/.config/ackrc', '-H',
        \ '--nopager', '--nocolor', '--nogroup', '--column'])
endif

" KEY MAPPINGS
let insert_mode_mappings = [
      \  ['jj', '<denite:enter_mode:normal>', 'noremap'],
      \  ['`', '<denite:enter_mode:normal>', 'noremap'],
      \  ['<C-J>', '<denite:assign_next_matched_text>', 'noremap'],
      \  ['<C-K>', '<denite:assign_previous_matched_text>', 'noremap'],
      \  ['<Up>', '<denite:assign_previous_text>', 'noremap'],
      \  ['<Down>', '<denite:assign_next_text>', 'noremap'],
      \  ['<C-Y>', '<denite:redraw>', 'noremap'],
      \  ['<BS>', '<denite:smart_delete_char_before_caret>', 'noremap'],
      \  ['<C-h>', '<denite:smart_delete_char_before_caret>', 'noremap'],
      \  ['<C-c>', '<denite:quit>', 'noremap'],
      \  ['<esc>', '<denite:quit>', 'noremap'],
      \ ]

let normal_mode_mappings = [
      \  ["'", '<denite:toggle_select_down>', 'noremap'],
      \  ['<C-j>', '<denite:jump_to_next_source>', 'noremap'],
      \  ['<C-k>', '<denite:jump_to_previous_source>', 'noremap'],
      \  ['gg', '<denite:move_to_first_line>', 'noremap'],
      \  ['st', '<denite:do_action:tabopen>', 'noremap'],
      \  ['sg', '<denite:do_action:vsplit>', 'noremap'],
      \  ['sv', '<denite:do_action:split>', 'noremap'],
      \  ['sc', '<denite:quit>', 'noremap'],
      \  ['<C-c>', '<denite:quit>', 'noremap'],
      \  ['<esc>', '<denite:quit>', 'noremap'],
      \  ['`', '<denite:enter_mode:insert>', 'noremap'],
      \  ['r', '<denite:redraw>', 'noremap'],
      \ ]

for m in insert_mode_mappings
  call denite#custom#map('insert', m[0], m[1], m[2])
endfor
for m in normal_mode_mappings
  call denite#custom#map('normal', m[0], m[1], m[2])
endfor

