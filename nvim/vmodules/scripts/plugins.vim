" Scripts plugins

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-abolish'
Plug 'godlygeek/tabular'
Plug 'guns/vim-sexp'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'wellle/targets.vim'
