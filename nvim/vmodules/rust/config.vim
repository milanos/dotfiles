" Rust config

let g:rust_clip_command = 'xclip -selection clipboard'
let g:rustfmt_autosave = 1

" let g:racer_experimental_completer = 1

" let g:racer_cmd = "/home/gem/.cargo/bin/racer"

au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap <leader>gd <Plug>(rust-doc)
