" Wiki config

func! WordProcessorMode()
  setlocal formatoptions=1
  setlocal noexpandtab
  map j gj
  map k gk
  setlocal spell spelllang=en_us
  set complete+=s
  set formatprg=par
  setlocal wrap
  setlocal linebreak
endfu
com! WP call WordProcessorMode()
au BufRead *.wiki :WP
let g:vimwiki_list = [
    \ { 'path': '~/Organized/wiki/main', 'path_html': '~/Organized/site/main', 'ext': '.wiki', 'auto_tags': 1 },
    \ { 'path': '~/Organized/wiki/code', 'path_html': '~/Organized/site/code', 'ext': '.wiki', 'auto_tags': 1 },
    \ { 'path': '~/Organized/wiki/school', 'path_html': '~/Organized/site/school', 'ext': '.wiki', 'auto_tags': 1 },
    \]

