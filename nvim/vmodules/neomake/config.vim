" Neomake config

" When writing a buffer (no delay).
call neomake#configure#automake('w')

let g:neomake_java_javac_maker = {
        \ 'errorformat':
            \ '%E%f:%l: %trror: %m,' .
            \ '%W%f:%l: %tarning: %m,' .
            \ '%E%f:%l: %m,'.
            \ '%Z%p^,'.
            \ '%-G%.%#',
        \ }
