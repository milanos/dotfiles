" Wordprecessing config
"
let g:polyglot_disabled = ['latex']

let g:tq_enabled_backends=["thesaurus_com","mthesaur_txt", "openoffice_en"]
let g:tq_online_backends_timeout = 1
" Spellcheck for certian files
func! WordProcessorMode()
  setlocal formatoptions=1
  setlocal noexpandtab
  map j gj
  map k gk
  setlocal spell spelllang=en_us
  set complete+=s
  set formatprg=par
  setlocal wrap
  setlocal linebreak
endfu
com! WP call WordProcessorMode()
au BufRead *.txt :WP
au BufRead *.md :WP
au BufRead *.markdown :WP
let g:notes_suffix = '.txt'
let g:notes_directories = ['~/Organized/notes']
let g:vimtex_compiler_progname = 'rubber'
nnoremap <leader>s [s1z=``
nnoremap <leader>S ]s1z=``

au BufNewFile,BufRead *.mom			set filetype=mom spell

