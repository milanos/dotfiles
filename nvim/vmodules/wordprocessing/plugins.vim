" Wordprecessing plugins

Plug 'ron89/thesaurus_query.vim'
" Plug 'rhysd/vim-grammarous'
Plug 'lervag/vimtex'
" Plug 'vim-latex/vim-latex'
Plug 'plasticboy/vim-markdown'
" Plug 'vim-pandoc/vim-pandoc'

" For note taking
Plug 'xolox/vim-notes'
Plug 'xolox/vim-misc'
" Plug 'retorillo/jekyll-util.vim'
Plug 'vim-scripts/mom.vim'
