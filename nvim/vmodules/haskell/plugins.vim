"" Haskell Bundle
Plug 'eagletmt/neco-ghc'
Plug 'dag/vim2hs'
" Plug 'eagletmt/ghcmod-vim'
Plug 'bitc/vim-hdevtools'
" Plug 'Shougo/vimproc.vim'
" Plug 'm00qek/vim-pointfree'
Plug 'lukerandall/haskellmode-vim'
