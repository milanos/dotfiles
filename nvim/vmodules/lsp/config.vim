" Lsp config

" let g:lsc_server_commands = {'elixir': 'elixir-language-server'}
" let g:lsc_enable_autocomplete = v:false
" let g:lsc_auto_map = {
"     \ 'GoToDefinition': 'gf',
"     \ 'FindReferences': 'gr',
"     \ 'NextReference': '[l',
"     \ 'PreviousReference': ']l',
"     \ 'FindImplementations': 'gI',
"     \ 'FindCodeActions': 'ga',
"     \ 'DocumentSymbol': 'go',
"     \ 'WorkspaceSymbol': 'gS',
"     \ 'ShowHover': 'H',
"     \ 'Completion': 'completefunc',
"     \}

let g:LanguageClient_autoStart = 0
nnoremap <leader>lcs :LanguageClientStart<CR>

" if you want it to turn on automatically
" let g:LanguageClient_autoStart = 1

let g:LanguageClient_serverCommands = {
    \ 'python': ['pyls'],
    \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
    \ 'javascript': ['javascript-typescript-stdio'],
    \ 'go': ['go-langserver'],
    \ 'elixir': ['elixir-language-server']
    \}

noremap <silent> <Leader>H :call LanguageClient_textDocument_hover()<CR>
noremap <silent> <Leader>Z :call LanguageClient_textDocument_definition()<CR>
noremap <silent> <Leader>R :call LanguageClient_textDocument_rename()<CR>
noremap <silent> <Leader>S :call LanugageClient_textDocument_documentSymbol()<CR>
noremap <silent> <Leader>I :call LanugageClient_textDocument_implementation()<CR>
