" Ultisnips plugins

if has("python3")
  Plug 'SirVer/ultisnips'
endif

Plug 'honza/vim-snippets'
