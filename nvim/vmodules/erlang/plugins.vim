" Erlang plugins

Plug 'edkolev/erlang-motions.vim'
Plug 'vim-erlang/vim-erlang-compiler'
Plug 'vim-erlang/vim-erlang-omnicomplete'
Plug 'vim-erlang/vim-erlang-skeletons'
Plug 'vim-erlang/vim-erlang-tags'
Plug 'vim-erlang/vim-erlang-runtime'
" For conceals
" Plug 'ehamberg/vim-cute-erlang'

" Plug 'tpope/vim-dispatch'
