" Gundoo config
nnoremap <F5> :GundoToggle<CR>
let g:gundo_width=30
let g:gundo_preview_height=10
let g:gundo_preview_bottom=10
let g:gundo_right=1
