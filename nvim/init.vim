"                                 ___     
"        ___        ___          /__/\    
"       /__/\      /  /\        |  |::\   
"       \  \:\    /  /:/        |  |:|:\  
"        \  \:\  /__/::\      __|__|:|\:\ 
"    ___  \__\:\ \__\/\:\__  /__/::::| \:\
"   /__/\ |  |:|    \  \:\/\ \  \:\~~\__\/
"   \  \:\|  |:|     \__\::/  \  \:\      
"    \  \:\__|:|     /__/:/    \  \:\     
"     \__\::::/      \__\/      \  \:\    
"         ~~~~                   \__\/    

 " Set compatibility to Vim only.
set nocompatible

"Always show current position
set ruler

" Turn on syntax highlighting.
syntax on

" Uncomment below to set the max textwidth. Use a value corresponding to the width of your screen.
" set textwidth=80
set formatoptions=tcqrn1
set softtabstop=4
set shiftwidth=0
set tabstop=4
set noexpandtab
set noshiftround

" Turn off modelines
set modelines=0

" Allow hidden buffers
set hidden

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start

" Display options
set showmode
set showcmd
set cmdheight=1

" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

"Autoread changes in a file
set autoread

" Display different types of white spaces.
"set list
"set listchars=tab:›\ ,trail:•,extends:#,nbsp:.

" Show line numbers
set number
set relativenumber
highlight LineNr ctermfg=black

" Set status line display
set laststatus=2
hi StatusLine ctermfg=NONE ctermbg=red cterm=NONE
hi StatusLineNC ctermfg=black ctermbg=red cterm=NONE
hi User1 ctermfg=black ctermbg=magenta
hi User2 ctermfg=NONE ctermbg=NONE
hi User3 ctermfg=black ctermbg=blue
hi User4 ctermfg=black ctermbg=cyan
set statusline=\                    " Padding
set statusline+=%f                  " Path to the file
set statusline+=\ %1*\              " Padding & switch colour
set statusline+=%y                  " File type
set statusline+=\ %2*\              " Padding & switch colour
set statusline+=%=                  " Switch to right-side
set statusline+=\ %3*\              " Padding & switch colour
set statusline+=line                " of Text
set statusline+=\                   " Padding
set statusline+=%l                  " Current line
set statusline+=\ %4*\              " Padding & switch colour
set statusline+=of                  " of Text
set statusline+=\                   " Padding
set statusline+=%L                  " Total line
set statusline+=\                   " Padding

" Encoding
set encoding=utf-8

" Highlight matching search patterns
set hlsearch

" Enable incremental search
set incsearch

" Enable substitution highlight
set inccommand=nosplit

" Include matching uppercase words with lowercase search term
set ignorecase

" Include only uppercase words with uppercase search term
set smartcase

" Store info from no more than 100 files at a time, 9999 lines of text
" 100kb of data. Useful for copying large amounts of data between files.
set viminfo='100,<9999,s100

" Map the leader key to ,
let mapleader = ','

" Copy and pasting
set clipboard^=unnamedplus
noremap x "_x
noremap X "_X

" visual mode indents
vmap > >gv
vmap < <gv

" quick macros
vmap Q @q
nmap Q @q

" Turns off highlights
map <CR> :noh <cr>

" Remove all training whitespace
nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>

" Sudo edit a file quickly
cmap w!! w !doas tee > /dev/null %

" For easy increments
vmap <C-a> <C-a>gv
vmap <C-x> <C-x>gv

" Folds
set foldmethod=marker

" Shift Errors
cabbrev Q q
cabbrev W w
cabbrev X x

" Normal mode man pages
nmap K viwK

" Spell fixes
nnoremap <leader>s [s1z=``
nnoremap <leader>S ]s1z=``


call plug#begin()

"Plug 'https://github.com/vimwiki/vimwiki'
	Plug 'terryma/vim-multiple-cursors'
	Plug 'tpope/vim-repeat'
	Plug 'dylanaraps/wal.vim'
	Plug 'itchyny/lightline.vim'
	Plug 'edkolev/tmuxline.vim'

	Plug 'christoomey/vim-tmux-navigator'

	Plug 'tpope/vim-commentary'
	Plug 'tpope/vim-surround'
	Plug 'godlygeek/tabular'
	Plug 'AndrewRadev/splitjoin.vim'

	Plug 'tpope/vim-eunuch'
	Plug 'tpope/vim-abolish'

	Plug 'tpope/vim-unimpaired'
	Plug 'guns/vim-sexp'
	Plug 'wellle/targets.vim'

	Plug 'majutsushi/tagbar'
	" Plug 'ludovicchabant/vim-gutentags'

	Plug 'tpope/vim-fugitive'
	Plug 'airblade/vim-gitgutter'

	Plug 'vim-syntastic/syntastic'

	Plug 'rust-lang/rust.vim'
	Plug 'racer-rust/vim-racer'

	Plug 'plasticboy/vim-markdown'

call plug#end()

" Some Colorscheme choices
colorscheme wal
hi Comment ctermbg=NONE ctermfg=8 cterm=italic
hi! link NonText LineNr
hi MatchParen cterm=bold ctermbg=none ctermfg=RED
hi VertSplit ctermbg=blue ctermfg=black
hi CursorLine cterm=bold ctermbg=NONE ctermfg=NONE

let g:lightline = {
      \ 'colorscheme': 'wal',
      \ }


" For nice ctag support

let g:gutentags_cache_dir = '~/.tags_cache'

nmap <F8> :TagbarToggle<CR>
let g:tagbar_type_haskell = {
    \ 'ctagsbin'  : 'hasktags',
    \ 'ctagsargs' : '-x -c -o-',
    \ 'kinds'     : [
        \  'm:modules:0:1',
        \  'd:data: 0:1',
        \  'd_gadt: data gadt:0:1',
        \  't:type names:0:1',
        \  'nt:new types:0:1',
        \  'c:classes:0:1',
        \  'cons:constructors:1:1',
        \  'c_gadt:constructor gadt:1:1',
        \  'c_a:constructor accessors:1:1',
        \  'ft:function types:1:1',
        \  'fi:function implementations:0:1',
        \  'o:others:0:1'
    \ ],
    \ 'sro'        : '.',
    \ 'kind2scope' : {
        \ 'm' : 'module',
        \ 'c' : 'class',
        \ 'd' : 'data',
        \ 't' : 'type'
    \ },
    \ 'scope2kind' : {
        \ 'module' : 'm',
        \ 'class'  : 'c',
        \ 'data'   : 'd',
        \ 'type'   : 't'
    \ }
\ }

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0


" Rust
let g:rust_clip_command = 'xclip -selection clipboard'
au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap K  <Plug>(rust-doc)

" Bar
let g:tmuxline_powerline_separators = 0

" Ultisnips config


let g:UltiSnipsEditSplit="vertical"

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" Python
let g:syntastic_python_checkers=['python', 'python3']
au BufRead,BufNewFile *.py set expandtab

" Vim wiki
let g:vimwiki_list = [{'path': '~/Code/company/journal',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
