nnoremap <leader>lt :ErlangTags<CR>

set expandtab
set tabstop=2
set softtabstop=2

nmap K viw<Plug>(ref-keyword)
